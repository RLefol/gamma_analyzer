#ifndef GAMMA_ANALYZER_H
#define GAMMA_ANALYZER_H

/*	Tree				*/
#define  class_h101_cxx

#include "classes/class_h101.h"
//#include "classes/scaler.h"
//#include "libs/scaler_lib.h"

/*	Input/Output		*/
#include <iostream>
#include <fstream>

/*	Container			*/
#include <vector>

/*	Root				*/
#include <TH1.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "TChain.h"
#include "TString.h"
#include "TFile.h"
#include "TLegend.h"
#include <TRandom3.h>
#include <TStopwatch.h>
#include "TLine.h"
#include "TGraphErrors.h"
#include "TLatex.h"
#include "TMath.h"

/*	My Libraries		*/
#include "../c_files/gate/gate.h"					            // implements class gates
#include "../c_files/text_output/colored_text.h"	    // defines colores for text output
#include "../c_files/text_output/colored_text_lib.h"  // additional functions for colored text output
#include "../c_files/progress_output/progress_output.h"  // additional functions for colored text output


/*	Structures			*/
// structure to save data that are later written to T_data_g
struct event
{
    UInt_t   ev_index;		// number of electron trigger event in this multiplicity
    Double_t ev_time_stamp;	// save time stamp
    Double_t ev_energy;		// save energy
};


/*	Initialization		*/
// initialize arrays with zeros
void init_arrays_0();
// initialize canvas if it is defined
void init_canvas();
// initialization
void init(const TString energy_cal_file, const TString experiment);

/*	Trees			*/
// initialize branches for all trees
void init_branches(TTree* T_data_g, TTree* T_trig_g);
// initialize gamma tree
void init_branches_g(TTree* tree);
// initialize gamma electron trigger tree with time differences
void init_branches_trig(TTree* tree);

// fill event data into events structure
void fill_events(UInt_t channel);
// fill data into tree
void fill_tree(TTree* tree);

/*	Time Stamps		*/
//calculate time stamps
Long64_t calc_time_stamp_full(UInt_t time_stamp_Hi_v, UInt_t time_stamp_Lo_v);
//calculate corrected timestamps
Double_t calc_time_stamp_corr(UInt_t time_stamp_Hi_v, UInt_t time_stamp_Lo_v, UInt_t max, UInt_t after, UInt_t before);
//calculate time difference between electron and gamma
void calc_coin_time_diff(UInt_t channel);

/*  Energy    */
//import energy calibration data
void import_energy_calibration(const TString energy_cal_file, const TString experiment);
//energy calibration
Double_t energy_calibration(UInt_t maxEnergyv, UInt_t chv);

/*	Rate			*/
//calculate average rate and reset rate
void calc_rate();
//print average rate
void calc_rate_avg();
// calculate rate in time interval [rate_interval_tstart,rate_interval_tstop]
void calc_rate_interval();
// perform calculation for rate in interval
void calc_rate_interval_sum(Bool_t* status, UInt_t *rate_interval_counts, Double_t *time_index);

/*  Print     */
// print rate in time interval
void rate_interval_print();
// print number of pileups for all channels
void print_pileup_info();
// print deadtime
void print_deadtime();
// print deadtime in the gate
void print_deadtime_gate();
// print scaler information
void print_scaler_info();
// print computing times
void print_time();
// print information
void print();

/* Scaler			*/
// calculate scaler data
void calc_scaler_data(Double_t* n_int, Double_t* n_hit, Double_t* n_dead, Double_t* n_pileup, Double_t* n_veto, Double_t* n_he, UInt_t i);
// calculate scaler counts since start of the measurement
inline Double_t calc_scaler_val(UInt_t scaler, UInt_t scaler_start, UInt_t scaler_over, Double_t n_32){ return (((Double_t)(scaler - scaler_start)) + ((Double_t)scaler_over * n_32));};

/*	Histograms	*/
// define histogram properties
void define_hist_properties();

/*	Canvas					*/
// fill canvas energies
void fill_canvas_energy();
// fill canvas rates
void fill_canvas_rate();
// fill canvas time differences
void fill_canvas_time_diff();
// fill canvas coincidence data
void fill_canvas_coin();
// fill canvas time difference between trigger
void fill_trig_time_diff();
// fill canvas other histograms
void fill_canvas_other();
// fill canvas energy 2D
void fill_canvas_energy_2D();
// fill canvas scaler data
void fill_canvas_scaler();
// fill canvas with histograms
void fill_canvas();

/*	Export Data		*/
// export trees
void export_trees(TTree* tree, TString file_name, TString type, TString description);
// export canvas
void export_canvas(TCanvas* canvas, TString file_name, TString type);
// export H1 as ascii table
void H1_to_ascii(TH1F* hist, TString file_name);
// export H1 as .root
void H1_to_root(TH1F* H1_hist, TString file_name);
// export Canvas as .root
void C_to_root(TCanvas* C_canvas, TString file_name);
// write data, histograms and ... to files
void export_data(TString file_name, TCanvas* C_energy, TCanvas* C_rate, TCanvas* C_coin);


#endif
